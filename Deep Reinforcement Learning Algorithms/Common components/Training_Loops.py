from Required_Imports import TrainPlay
from Network_Read_Write import *


def off_policy_training_loop(agent, env, train_or_play, training_steps=10000, algorithm_name="Temp"):
    """
    The Agent is trained over a certain amount of training steps.
    :param train_or_play: Should just play the game or train network
    :param algorithm_name: Name of the algorithm.
    :param agent: The Agent with the model and target DQN.
    :param env: The game environment.
    :param training_steps: Numer of training steps.
    :return:
    """

    # Set up the starting state of the agent.
    lengths, rewards = [], []
    state = env.reset()[0]
    ep_score, ep_steps = 0, 0
    for t in range(1, training_steps):
        # Select action according to the model's forward pass or explored sample.
        action = agent.act(state, env.action_space)
        # Receive the new state and reward according to the action's outcome.
        next_state, reward, done, _, info = env.step(action)
        # Update and append the memory buffer.
        agent.replay_buffer.update_memory(state, action, next_state, reward, done)
        if ep_steps + 1 == 500:
            done = True
        ep_score += reward
        ep_steps += 1
        state = next_state

        if train_or_play == TrainPlay.Train:
            agent.train()

        if done:
            state, done = env.reset()[0], False
            print("Step: {}, Episode :{}, Score : {:.1f}".format(t, len(lengths), ep_score))
            ep_score, ep_steps = 0, 0

    if train_or_play == TrainPlay.Train:
        save_model(agent.model, agent.optimizer, algorithm_name)
