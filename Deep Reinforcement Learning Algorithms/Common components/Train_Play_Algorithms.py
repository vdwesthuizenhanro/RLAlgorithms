import gym
from DQN import *
from Training_Loops import *


def train_or_play_dqn(environment="CartPole-v0", train_or_play=TrainPlay.Train, steps=10000):
    """
        Initialise the gym environment and start the off policy training loop.
        DQN is an off policy agent:
        The reason that Q-learning is off-policy is that it updates its Q-values using
        the Q-value of the next state 𝑠′ and the greedy action 𝑎′.
    """
    env = gym.make(environment)
    state_dim = env.observation_space.shape[0]
    n_acts = env.action_space.n

    agent = DQNAgent(state_dim, n_acts)
    if train_or_play == TrainPlay.Play:
        state_dict, optimizer = load_model(algorithm_name="DQN")
        agent.model.load_state_dict(state_dict)
        agent.optimizer.load_state_dict(optimizer)
        env = gym.make(environment, render_mode="human")

    off_policy_training_loop(agent, env, train_or_play, steps, "DQN")


if __name__ == "__main__":
    train_play = input("Train (0) or Play (1): ")
    if train_play[0] == "0":
        train_play = TrainPlay.Train
    else:
        train_play = TrainPlay.Play
    train_or_play_dqn(train_or_play=train_play)
