import torch


def save_model(model_state, optimizer_state, algorithm_name="Temp"):
    print("Save state of {}".format(algorithm_name))
    state_dict = {"state_dict": model_state, "optimizer": optimizer_state}
    torch.save(state_dict, "../Network_State_Dict/{}.tar".format(algorithm_name))


def load_model(algorithm_name):
    state_dict = torch.load("../Network_State_Dict/{}.tar".format(algorithm_name))
    return state_dict["state_dict"].state_dict(), state_dict["optimizer"].state_dict()
