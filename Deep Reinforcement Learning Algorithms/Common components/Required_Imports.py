from enum import Enum


class TrainPlay(Enum):
    Train: int = 0
    Play: int = 1
