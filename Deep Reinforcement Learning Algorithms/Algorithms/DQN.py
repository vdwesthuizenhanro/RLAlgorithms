import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import random
from Policy_Buffer import OffPolicyBuffer

HIDDEN_LAYER_1 = 400
HIDDEN_LAYER_2 = 300
MEMORY_SIZE = 100000

GAMMA = 0.94
LEARNING_RATE = 0.001
BATCH_SIZE = 32

EXPLORATION_MAX = 1.0
EXPLORATION_MIN = 0.01
EXPLORATION_DECAY = 0.995


class DeepQNetwork(nn.Module):
    def __init__(self, obs_space, action_space):
        super(DeepQNetwork, self).__init__()
        self.fc1 = nn.Linear(obs_space, HIDDEN_LAYER_1)
        self.fc2 = nn.Linear(HIDDEN_LAYER_1, HIDDEN_LAYER_2)
        self.fc3 = nn.Linear(HIDDEN_LAYER_2, action_space)

    def forward(self, x):
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x


class DQNAgent:
    def __init__(self, obs_space, action_space):
        """
        Initialise the Agent that makes use of the DQN.
        :param obs_space: Characteristics of the current state of the environment.
        :param action_space: Actions that can e taken.
        """
        self.obs_space = obs_space
        self.action_space = action_space
        self.replay_buffer = OffPolicyBuffer(obs_space, 1)

        self.model = DeepQNetwork(obs_space, action_space)
        self.target = DeepQNetwork(obs_space, action_space)
        self.optimizer = optim.Adam(self.model.parameters(), lr=LEARNING_RATE)

        self.exploration_rate = EXPLORATION_MAX
        self.update_steps = 0

    def act(self, state, actions):
        """
        :param state: state: The current input state variables for the DQN.
        :param actions: All possible actions.
        :return: Either return a random sampled action or perform forward pass and select the
        action corresponding to the highest q value.
        """
        if random.random() < self.exploration_rate:
            return actions.sample()
        else:
            state = torch.tensor(state).float()
            q_values = self.model.forward(state)
            action = q_values.argmax().item()
            return action

    def train(self):
        if self.replay_buffer.size() < BATCH_SIZE:
            return
        state, action, next_state, reward, done = self.replay_buffer.sample(BATCH_SIZE)

        next_values = self.target.forward(next_state)
        max_vals = torch.max(next_values, dim=1)[0].reshape((BATCH_SIZE, 1))
        q_target = reward + GAMMA * max_vals * done
        q_vals = self.model.forward(state)
        current_q_a = q_vals.gather(1, action.type(torch.int64))

        loss = torch.nn.functional.mse_loss(current_q_a, q_target.detach())
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        self.update_steps += 1
        if self.update_steps % 256 == 1:
            self.target.load_state_dict(self.model.state_dict())
        if self.update_steps % 12 == 1:
            self.exploration_rate *= EXPLORATION_DECAY
            self.exploration_rate = max(EXPLORATION_MIN, self.exploration_rate)

        if self.update_steps % 1000 == 1:
            print("Exploration rate: ", self.exploration_rate)