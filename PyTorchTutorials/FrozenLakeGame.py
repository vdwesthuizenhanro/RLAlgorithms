import numpy as np
import gym
import random
import time

env = gym.make("FrozenLake-v1")

actionSpaceSize = env.action_space.n
stateSpaceSize = env.observation_space.n

q_table = np.zeros((stateSpaceSize, actionSpaceSize))
#  print(q_table)  # The state is the column and the action is the row.

episodes = 1000
maxStepsPerEpisode = 100
learningRate = 0.1
discountRate = 0.99
explorationRate = 1
maxExplorationRate = 1
minExplorationRate = 0.01
explorationDecayRate = 0.001

episodeRewards = []

# Q-Learning algo
for episode in range(episodes):
    state = env.reset()[0]
    done = False
    episodeReward = 0

    for step in range(maxStepsPerEpisode):
        explorationRateThreshold = random.uniform(0, 1)
        if explorationRateThreshold > explorationRate:
            action = np.argmax(q_table[state, :])  # Action is 1 to 4 chose highest Q-value at given state.
        else:
            action = env.action_space.sample()

        newState, reward, done, _, info = env.step(action)

        # Formula add: the max Q value of the newStates action pair.
        q_table[state, action] = q_table[state, action] * (1 - learningRate) + \
                                 learningRate * (reward + discountRate * np.max(q_table[newState, :]))

        state = newState
        episodeReward = reward

        if done:
            break

    explorationRate = minExplorationRate + \
                      (maxExplorationRate - minExplorationRate) * np.exp(-explorationDecayRate * episode)
    episodeRewards.append(episodeReward)

splitRewards = np.split(np.array(episodeRewards), episodes/1000)
count = 1000
print("Average rewards")
for i in splitRewards:
    print("{}: {}".format(count, sum(i/1000)))
    count += 1000

print("Q Table")
print(q_table)
env.close()

env = gym.make("FrozenLake-v1", render_mode = "human")
for i in range(3):
    state = env.reset()[0]
    done = False
    print("Epsisode {} \n\n".format(i+1))
    time.sleep(1)
    for step in range(maxStepsPerEpisode):
        env.render()
        time.sleep(0.3)

        action = np.argmax(q_table[state,:])
        newState, reward, done, _, info = env.step(action)

        if done:
            break
        state = newState
env.close()